/**
 * Created by Juan on 10/04/16.
 */

'use strict';

const argv = require('minimist')(process.argv.slice(2));
const fs   = require('fs');
const path = require('path');

function getType(object) {
	return Object.prototype.toString.call(object);
}

function exposeConfig(config) {

	// start walking
	walk(config);

	function exposeKey(path) {

		function expose(object) {
			let arg = argv[path.join('-')];
			if(!arg) return;

			// handle comma separated values as arrays
			if(getType(arg) === '[object String]') arg = arg.indexOf(',') === -1 ? arg : arg.split(',');

			// step into the right key path
			const key = path.length - 1;
			for(let i = 0; i < key; i++) object = object[path[i]];

			let value = arg || object[path[key]];

			// handle booleans
			value = (value === 'true' || value === 'false') ? Boolean(value) : value;

			// assign the passed argument to the config object
			object[path[key]] = value;
		}

		expose(config);
	}

	function walk(object, path) {
		path = path || [];

		Object.keys(object).forEach(key => {
			// register key
			path.push(key);

			// recursively call in case of objects
			if(getType(object[key]) === '[object Object]') walk(object[key], path);

			// expose key
			exposeKey(path);

			// get back to the root object
			path.pop();
		});

		// clean the path
		path.length = 0;
	}

	// return the new config to allow chaining
	return config;
}

module.exports.expose = exposeConfig;

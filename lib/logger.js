/**
 * Created by Juan on 26/02/16.
 */

'use strict';

const winston = require('winston');
const path    = require('path');
const fs      = require('fs');
const folder  = `${path.dirname(require.main.filename)}/logs`;

// create log folder if it doesn't exists
if(!fs.existsSync(folder)) fs.mkdirSync(folder);

module.exports = function(level, consoleOnly) {
  const transports = [
    new winston.transports.Console({
      level:     level,
      colorize:  true,
      timestamp: true
    })
  ];

  if(consoleOnly) transports.concat(
      new winston.transports.File({
        name:      'log',
        filename:  `${folder}/process.log`,
        level:     level,
        json:      false,
        timestamp: true
      }),
      new winston.transports.File({
        name:        'error',
        filename:    `${folder}/error.log`,
        level:       'error',
        prettyPrint: true,
        json:        false,
        timestamp:   true
      }));

  return new winston.Logger({
    transports: transports
  });

};

/**
 * Created by Juan on 17/04/16.
 */

'use strict';

exports.benchmark = function() {
  const start = process.hrtime();

  function toTimeString(seconds) {
    return (new Date(seconds * 1000)).toUTCString().match(/(\d\d:\d\d:\d\d)/)[0];
  }

  return function(mode) {
    mode               = mode || 'string';
    const end          = process.hrtime(start);
    const time         = `${toTimeString(end[0])}:${Math.round(end[1] / 1000000)}`.split(':');
    const hours        = parseInt(time[0]);
    const minutes      = parseInt(time[1]);
    const seconds      = parseInt(time[2]);
    const milliseconds = parseInt(time[3]);

    if (mode === 'time') return time.join(':');
    if (mode === 'object') return {hours, minutes, seconds, milliseconds};

    const result = [];
    if (hours !== 0) result.push(`${hours}h`);
    if (minutes !== 0) result.push(`${minutes}m`);
    if (seconds !== 0) result.push(`${seconds}s`);
    if (milliseconds !== 0) result.push(`${milliseconds}ms`);

    return result.join(' ');
  }
};

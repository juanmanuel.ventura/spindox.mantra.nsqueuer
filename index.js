/**
 * Created by Juan on 15/04/16.
 */

'use strict';

const nsq  = require('nsq.js');
const util = require('./lib/util');

module.exports = function(options) {
  options = require('./lib/dynamic_config').expose(options);

  options.logging = options.logging || {};

  const logger = require('./lib/logger')(options.logging.level || 'info', options.logging.consoleOnly || true);

  // manage mandatory fields
  if (!options.nsq) throw Error('no [nsq] object found in the options');
  if (!options.nsq.topic) throw Error('nsq.topic must be passed in the options');
  if (!options.nsq.channel) throw Error('nsq.channel must be passed in the options');

  // apply NSQ defaults
  options.nsq = Object.assign({
    nsqd:        ['127.0.0.1:4150'],
    maxInFlight: 100,
    maxAttempts: 5
  }, options.nsq);

  return {
    connect(callback) {
      let reader = nsq.reader(options.nsq);
      let writer = nsq.writer(options.nsq.nsqd[0]);

      writer.on('error', handleError.bind('writer'));
      reader.on('error', handleError.bind('reader'));
      writer.on('error response', handleError.bind('writer'));
      reader.on('error response', handleError.bind('reader'));

      writer.on('ready', function() {
        logger.log('info', `[${process.pid}] connected to nsq in ${options.nsq.nsqd}`);

        reader.on('discard', function(message) {
          writer.publish('garbage', {
            reason:  'discarded',
            payload: message.json()
          }, function(err) {
            if (err) throw err;
            logger.log('error', `[${process.pid}] giving up on \n${JSON.stringify(message.json(), null, 2)}`);
            message.finish();
          });
        });

        callback({publish: writer.publish.bind(writer), on: reader.on.bind(reader)});
      });
    },

    log(level, message) {
      logger.log(level, message);
    },

    benchmark: util.benchmark,
    config:    options
  };

  function handleError(err) {
    logger.log('error', `[${process.pid}] nsq error: ${this} \n${JSON.stringify(err, null, 2)}`);
    process.exit(1);
  }
};



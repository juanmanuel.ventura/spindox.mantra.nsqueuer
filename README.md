# NSQueuer

A small NSQ wrapper with default behaviour

## Features

- simple single purpose wrapper around nsq client
- single subscription (only one `on('message')` handler)
- single publisher
- single configuration on instantiation
- includes logging
- default error handling

Usage:

```js
const NSQueuer = require('spindox.mantra.nsqueuer');
const app = NSQueuer({
  topic:   'myTopic',
  channel: 'myChannel'
});

app.connect(bus => {

  bus.on('message', message => {
    const payload = message.body.toString();
    app.log('info', payload);
    message.finish();
  });

  bus.publish('MyTopic', '123');
});

```


## API

### NSQueuer(options)

Creates a NSQueuer instance

Options:

- `topic` topic name
- `channel` channel name
- `nsqd` array of nsqd addresses
- `nsqlookupd` array of nsqlookupd addresses
- `maxInFlight` max messages distributed across connections [10]
- `maxAttempts` max attempts before discarding [5]

### app.connect(callback)
 
Connects to an NSQ instance, the callback gets passed a `bus` object 

### bus.on('message', callback)
- `message` (msg) incoming message

### bus.publish(topic, message, [callback])
 Publish the given `message` to `topic` where `message`
 may be a string, buffer, or object. 

## Message

 A single message.

### Message.finish()

  Mark message as complete.

### Message.requeue([delay])

  Re-queue the message immediately, or with the
  given `delay` in milliseconds, or a string such
  as "5s", "10m" etc.

### Message.touch()

  Reset the message's timeout, increasing the length
  of time before NSQD considers it timed out.

### Message.json()

  Return parsed JSON object.
